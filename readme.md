# Mr Escape

![Game Logo](/images/logo.png)

## Table of Contents

- [About the Game](#about-the-game)
- [Gameplay](#gameplay)
- [Features](#features)
- [Getting Started](#getting-started)
- [Installation](#installation)
- [Controls](#controls)

## About the Game

Mr Escape was my test project for second company. The game is a puzzle game with a physics engine. The game is written in Unity.

## Gameplay

The game is a puzzle game with a physics engine. The game is written in Unity. The game is a puzzle game with a physics
Goal is escape from the room.

## Features

- 3D graphics with optimized particles
- Open to use character with animations
- Mobile controls

## Getting Started

To download the game, go to the [releases page]

### Installation

1. Clone the repository: `git clone https://gitlab.com/unitysource/projects/mr-escape.git`
2. Navigate to the game directory: `cd mr-escape`
3. Install dependencies: `npm install` or `yarn install`

### Controls

Only mobile controls are available at the moment.

**Mobile Controls:**

- Joystick: Move
- Jump: By clicking the jump button, the character jumps.

## Acknowledgments

- [Sound effects by SoundBible](https://www.soundbible.com/)
